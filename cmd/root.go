package cmd

import cmd "gitlab.com/amrish.patel/libbeat/cmd"
import "github.com/comnetgmbh/cmkbeat/beater"

// Name of this beat
var Name = "cmkbeat"

// RootCmd to handle beats cli
var RootCmd = cmd.GenRootCmd(Name, "", beater.New)
